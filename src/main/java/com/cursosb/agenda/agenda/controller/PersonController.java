package com.cursosb.agenda.agenda.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cursosb.agenda.model.Person;
import com.cursosb.agenda.repository.PersonRepository;

@Controller
public class PersonController {
    @Autowired
    private PersonRepository repository;

    @GetMapping("/persons")
    public ModelAndView findAll() {
        ModelAndView mv = new ModelAndView("Person");
        mv.addObject("persons", repository.findAll());
        mv.addObject("person", new Person());
        return mv;
    }

    @PostMapping("/persons")
    public String save(Person person) {
        repository.save(person);
        return "redirect:/persons";
    }
}
