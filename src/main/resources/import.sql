-- create database persons;

insert into persons (name, email, birthday, active) values ('Fulano da Silva', 'fulano@gmail.com', '1982-10-04', true);
insert into persons (name, email, birthday, active) values ('Beltrano Gonçalves', 'beltranogol@gmail.com', '1981-10-04', true);
insert into persons (name, email, birthday, active) values ('Ciclano', 'ciclano@gmail.com', '1983-10-04', true);